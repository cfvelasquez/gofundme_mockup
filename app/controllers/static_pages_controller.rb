class StaticPagesController < ApplicationController
  def home
  end

  def contact
  end

  def campaigns
  end

  def comments
  end

  def donations
  end

  def login
  end

  def join
  end
  
end
