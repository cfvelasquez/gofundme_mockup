class CreateCampaigns < ActiveRecord::Migration[5.1]
  def change
    create_table :campaigns do |t|
      t.string :title
      t.string :description
      t.string :review
      t.string :goal
      t.string :category
      t.string :image_url
      t.string :creator
      t.integer :likes
      t.string :end_date

      t.timestamps
    end
  end
end
