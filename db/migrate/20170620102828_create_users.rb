class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :city
      t.string :country
      t.boolean :receive_email

      t.timestamps
    end
  end
end
