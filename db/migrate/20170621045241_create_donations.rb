class CreateDonations < ActiveRecord::Migration[5.1]
  def change
    create_table :donations do |t|
      t.string :author
      t.string :message
      t.integer :amount
      t.string :campaign

      t.timestamps
    end
  end
end
