Rails.application.routes.draw do
  get 'donations/new'

  get 'comments/new'

  get 'campaigns/new'

  root 'static_pages#home'

  get    '/contact',	  to: 'contacts#new'
  get    '/contact',    to: 'contacts#create'
  get    '/campaigns',	to: 'static_pages#campaigns'
  get    '/comments',	  to: 'static_pages#comments'
  get    '/donations',	to: 'static_pages#donations'
  get    '/signup',	    to: 'users#new'
  get    '/login',      to: 'sessions#new'
  post   '/login',      to: 'sessions#create'
  delete '/logout',     to: 'sessions#destroy'


  resources :users
  resources :contacts,  only: [:new, :create]
end
